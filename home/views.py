from django.shortcuts import render


from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.utils import simplejson
from django.core.mail import EmailMessage

from django import forms
from django.forms.widgets import Textarea

from home.models import *


def home(request):
	return render(request, 'home.html', {
		
	})

def home_candidato(request):
	return render(request, 'home_candidato.html', {
		
	})

def home_empresa(request):
	return render(request, 'home_empresa.html', {
		
	})

def visualizar_perfil(request):
	return render(request, 'visualizar_perfil.html', {
		
	})

def candidatos_selecionados(request):
	return render(request, 'candidatos_selecionados.html', {
		
	})

def busca_resultado(request):
	return render(request, 'busca_resultado.html', {
		
	})

def sobre(request):
	return render(request, 'sobre.html', {
		
	})

def saiba_mais(request):
	return render(request, 'saiba_mais.html', {
		
	})

def cadastro_candidato(request):
	return render(request, 'cadastro_candidato.html', {
		
	})

def cadastro_empresa(request):
	return render(request, 'cadastro_empresa.html', {
		
	})
