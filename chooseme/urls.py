from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'chooseme.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'home.views.home', name="home"),
    url(r'^home_candidato/', 'home.views.home_candidato', name="home_candidato"),
    url(r'^home_empresa/', 'home.views.home_empresa', name="home_empresa"),
    url(r'^visualizar_perfil/', 'home.views.visualizar_perfil', name="visualizar_perfil"),
    url(r'^candidatos_selecionados/', 'home.views.candidatos_selecionados', name="candidatos_selecionados"),
    url(r'^busca_resultado/', 'home.views.busca_resultado', name="busca_resultado"),
    url(r'^sobre/', 'home.views.sobre', name="sobre"),
    url(r'^cadastro_candidato/', 'home.views.cadastro_candidato', name="cadastro_candidato"),
    url(r'^cadastro_empresa/', 'home.views.cadastro_empresa', name="cadastro_empresa"),
    url(r'^saiba_mais/', 'home.views.saiba_mais', name="saiba_mais"),
)
